# Team Meeting

---
## Meeting Time
- Date: 2022.03.21
- Participants:
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - Analyzed the problems with the data itself and worked to provide the client with a SOW that could be signed
## Meeting Content:

### Topic 1: Focused discussions on technical difficulties
- Holder: Xixiang Chen
- Expect Time: 40 minutes

#### Discussion
This session focused on the current technical issues that arise. xixiang indicated that there are matching issues with IDs and that there are examples of mismatches if they are matched directly. Some IDs have hyphens, so some data doesn't match. Different links are just links to different pages on LinkedIn for the same person. xinjia said there are gaps in the recommendations section in some tables: not everyone has recommendations because they don't have work experience.
hongcong suggested that they consider copies of skills so that it is easier to search.
ruizheng said use part of the data for testing and part for training. There may not be enough data. Customers take into account and spend time preparing data or buying data. Suggest we cut some of the data for re-use. Two-way selection of data. Need to discuss and then provide data via email.
After negotiation with client, xinjia says assign tasks to SOW. it takes a couple of weeks for the IT agreement to be processed. Once they sign the agreement, the SOW will be given to us. Therefore, we invite the client to join our tutorials. Contact on Slack for more details. (Slack can have more effective communication.)

#### Action Points
| Action Items                                                                                                                      |     Person Responsible     |          Deadline |
|:----------------------------------------------------------------------------------------------------------------------------------|:--------------------------:|------------------:|
| In-depth research on algorithms                                        | Ruizheng Shen |  March 26, 2022 |
| Data pre-processing stage analysis                              | Hongcong Zhu | March 25, 2022 |
 | Related preparation for audit |  Chengyu Zhang  | March 25, 2022 |
 | Preparation for visualisation work                      |        Xigeng Li   |  March 25, 2022 |
Log writing |  Xixiang Chen  | March 24, 2022 |
 | Contact the client                  |         Xinjia Zhang    |  March 26, 2022 |

