# Team Meeting

---
## Meeting Time
- Date: 2022.03.17
- Participants:
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - The communication of technical problems on words to vectors.
## Meeting Content:

### Topic 1: Communicate the current problems including the deleting standard of data.
- Holder: Hongcong Zhu, Ruizheng Shen
- Expect Time: 72 minutes

#### Discussion
Based on what hongcong considers to be the deletion criteria for performing deleted data, xinjia and xixiang found that there were many variables with columns that had a blank rate higher than 70%. On the other hand, chengyu's evaluation revealed that there were many duplicate IDs and messages. xigeng added that in addition to this there was also useless information, such as some strange symbols. ruzheng suggested that we use python to pre-process the data first, store it in a data frame, link it up according to the primary key, and remove useless and confusing information. Then we can train the model.
#### Action Points
| Action Items                                                                                                                      |     Person Responsible     |          Deadline |
|:----------------------------------------------------------------------------------------------------------------------------------|:--------------------------:|------------------:|
| Data Extraction                                     | Ruizheng Shen,Hongcong Zhu |  March 28, 2022 |
| Data to word vectors                            | Chengyu Zhang. Xigeng Li | March 28, 2022 |
 | Data filtering |  Xinjia Zhang, Xixiang Chen  | March 28, 2022 |
 
