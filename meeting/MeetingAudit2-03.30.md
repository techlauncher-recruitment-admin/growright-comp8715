# Date
    2022.03.30
# Participants:
    Richard, Peter, Andrea
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
## Meeting objective：
    Report on project progress,  demonstrate that the team has been working effectively and delivering real value to our clients, 
    to each other, and to shadows. 

## Action Points From Audit 2:
- The presentation should start with an introduction to the project.

- The landing page is a bit cluttered and the documentation needs to be more concise.

- More documentation needs to be added to support the overall presentation of the project.