# Team Weekly Meeting

---
## Meeting Time
- Date: 2022.04.13
- Participants:
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - Improve the appearance of decision log and registration log
  - Assign researching tasks, Create template for research document
  - Reconstruct codes for data cleaning


## Meeting Content:

### Topic 1: Improve the Appearance of Decision Log and Registration Log
- Holder: Ruizheng Shen
- Expect Time: 10 minutes

#### Discussion
We have studied team Big Dream's documentation, and we decided to re-design the decision log. Ruizheng Shen concluded that our decision log was lack of severity category, Impact level 
category, reflection, proposer and approvement. For the contents, the decision is lack of evidence of decision making 
process. Xixiang Chen says that we actually had had decision making process, but we did not log them in the documentation. He 
would work with Xinjia on the decision log to add decision making process on the proposer category.
Xixiang Chen also suggests that we can ask for advice from our client, as they are more experienced in project management.

#### Action Points
| Action Items                                                                                  |     Person Responsible     |         Deadline |
|:----------------------------------------------------------------------------------------------|:--------------------------:|-----------------:|
| Add severity category, Impact level category, reflection, proposer and approvement categories | Xixiang Chen, Xinjia Zhang | 20th April, 2022 |
| Log decision making process in the decision log                                               | Xixiang Chen, Xinjia Zhang | 20th April, 2022 |


### Topic 2: Research on Recommendation Algorithm
- Holder: Ruizheng Shen
- Expect Time: 15 minutes

#### Discussion
Ruizheng Shen analysed the open source project of [Microsoft recommender]((https://github.com/microsoft/recommenders) and introduced 
how the recommendation system worked to team members.
He identified that SAR, NCF, BPR and RBM are the four algorithms that we could use, because they are widely used and easy
to implement and train. But we still need to explore whether the algorithm works with our data. He recommends Xigeng Li,
Chengyu Zhang, Hongcong Li and Ruizheng Shen to research on the algorithms, because they have experiences on machine learning.
Ruizheng Shen suggests that we should maintain research documents for every algorithms, because we need to have evidences to 
show our efforts to other stakeholders.

#### Action Points
| Action Items           | Person Responsible |         Deadline |
|:-----------------------|:------------------:|-----------------:|
| SAR algorithm research |     Xigeng Li      | 16th April, 2022 |
| RBM algorithm research |   Chengyu Zhang    | 16th April, 2022 |
| BPR algorithm research |    Hongcong Zhu    | 16th April, 2022 |
| NCF algorithm research |   Ruizheng Shen    | 16th April, 2022 |


#### Addition
- Tamplate  of research document:
    1. Motive/ Background of the model
    2. Technical support(what is neural network, etc)
    3. Algorithm description
    4. How to fit in our project
    5. Showcase/demo
    6. Conclusion

### Topic 3: Reconstruct Codes for Data cleaning
- Holder: Ruizheng Shen
- Expect Time: 5 minutes

#### Discussion
As we have discussed in April 2nd meeting, the data cleaning code should be re-construct into one file package and provide APIs.
Xinjia Zhang and Xixiang Chen will be responsible for this task.

#### Action Points
| Action Items                   |     Person Responsible     |         Deadline |
|:-------------------------------|:--------------------------:|-----------------:|
| reconstruct data cleaning code | Xixiang Chen, Xinjia Zhang |  16th April, 2022 |
