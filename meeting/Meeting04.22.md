# Client Meeting

---
## Meeting Time
- Date: 2022.04.02
- Meeting Holder: Ruizheng Shen
- Participants:
    Bryce Undy (Client)
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - Report our process to clients. Related agenda items:
    - Introduce the system architecture
    - Related method of data preprocessing
    - Researches for sorting layer(SAR, RBM, NCF, BPR)
  - Communicate to solve the data unsuitable problem.
  - Ask for suggestion of documentation

# Meeting Content:

### Topic 1: Report Our Process to Clients
Holder: Ruizheng Shen
Expect Time: 30 minutes

#### Discussion
Before the meeting, Bryce Undy has update on input data, which is PDF files of job description. Ruizheng Shen did a quick analyse 
on the new data but coming out result that this data could not be deployed in sorting layer. So the data suitable problem had not
yet been solved. Ruizheng Shen made a quick report to client about his opinion on the new data. And after the report, the meeting started.

Firstly, Hongcong Zhu introduced our design of the architecture and our pipeline of selecting suitable candidates. 

Then Ruizheng Shen introduced data preprocessing techs: tf-idf, word2vec and bert. Ruizheng Shen mentioned that tf-idf is not a machine learning 
method but works well; word2vec is a machine learning method, but we could use pre-trained model directly; bert is a machine learning 
method, but not works well in our model. Ruizheng Shen added that bert might be workable on the new data, but identified that,
we needed more than two weeks to deal with the new data, so we did not have time to implement that.

Then we reported on our research on recommending algorithms one-by-one. Ruizheng Shen concluded that all the algorithms 
need data of searching records. Bryce Undy asked for the amount of data needed. Ruizheng Shen answered that we would need more than one thousand
records. Ruizheng Shen also suggested that if the front-end could run online, we could obtain data from that. Bryce said we needed 
to upload the research document to project landing page so that he could share the document with his colleges.


#### Action Points
| Action Items                            |                  Person Responsible                   |         Deadline |
|:----------------------------------------|:-----------------------------------------------------:|-----------------:|
| Upload the research on landing page     |                     Xinjia Zhang                      | 23rd April, 2022 |


### Topic 2: Communicate to Solve the Data Unsuitable Problem
Holder: Ruizheng Shen
Expect Time: 20 minutes

#### Discussion
To how to solve the data unsuitable problem, Ruizheng Shen suggested that our model could actually work, but we did not have 
a place to deploy and see how it works. Ruizheng Shen also said that we could deliver this model as well as a report, which recorded
the overview of our project and analysis on the data we needed, so that client could have a better understanding on the project and data, and
probably provide usable data if the project continues.

#### Action Points
| Action Items              | Person Responsible |        Deadline |
|:--------------------------|:------------------:|----------------:|
| Write the delivery report |  All team members  |  11st May, 2022 |


### Topic 3: Ask for suggestion of documentation
Holder: Xixiang Chen
Expect Time: 10 minutes

#### Discussion
Bryce had a look at all of oiur documents. For the suggestion, Bryce said that our decision log was too operational, but we did not need to 
change the records in the decision log. The risk registration log should not 
contain 'severe risks' at this stage. Instead, the risk level should be gradually reduced. Bryce suggested that he could give us
templates of documents.

#### Action Points
| Action Items                                | Person Responsible |         Deadline |
|:--------------------------------------------|:------------------:|-----------------:|
| Improve the document based on the templates |  All team members  | 25th April, 2022 |
