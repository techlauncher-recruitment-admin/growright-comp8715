# Team Meeting

---
## Meeting Time
- Date: 2022.03.27
- Participants:
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - Dealing with different URL but for the same person in the recommendation list by using name to represent. 
  - Dealing with the problem that the URL in Recommendation list has some dupulicated data.

## Meeting Content:

### Topic 1: Assigning audit2's work
- Holder: Ruizheng Shen
- Expect Time: 40 minutes

#### Discussion
This session, chaired by ruizheng, focused on the distribution of work among audit2 individuals. First, based on the organised mind map provided by xixiang, we all learned about the audit2 process and the scoring requirements. hongcong suggested demonstrating technical achievements based on our existing data pre-processing and model training results, while chengyu suggested that teamwork and documentation should also be supplemented as appropriate. xigeng suggested that The materials we currently have are not enough to demonstrate the value of our project and we should communicate more with the client to ensure we are doing the right thing. After xinjia spoke to the client, we understood their suggestions and divided up the work based on their recommendations.


#### Action Points
| Action Items                                                                                                                      |     Person Responsible     |          Deadline |
|:----------------------------------------------------------------------------------------------------------------------------------|:--------------------------:|------------------:|
| Reflection                                        | Ruizheng Shen |  March 28, 2022 |
| Project output                                      | Hongcong Zhu | March 28, 2022 |
 | Teamwork |  Chengyu Zhang  | March 28, 2022 |
 | Stakeholder                      |        Xigeng Li   |  March 28, 2022 |
Risk/issue |  Xixiang Chen  | March 28, 2022 |
 | Next stage                      |         Xinjia Zhang    |  March 28, 2022 |

