# Team Meeting

---
## Meeting Time
- Date: 2022.04.06
- Participants:
    Ruizheng Shen, Hongcong Zhu
- Purpose:
  - Report on what've found in recommendation system.

## Meeting Content:

### Topic: What've found in recommendation system.
- Holder: Ruizheng Shen
- Expect time: 15 minutes

#### Discussion
Ruizheng Shen searched for recommendation system and discovered that the pipeline of recommending is similar to our designed, 
which included data preprocessing, similarity calculation and re-scoring. Ruizheng Shen said that we had already finished preprocessing steps and the 
processed data could be used to do similarity calculation, and we just needed a re-scoring algorithm to make the result more accurate.
So we can research on different re-scoring algorithms and find if there's a useable one. 
Ruizheng Shen found two open-source materials that would be useful, one is from [google colab](https://developers.google.com/machine-learning/recommendation), the other 
is from [Microsoft recommender](https://github.com/microsoft/recommenders). He suggests that we can learn from the coding style of microsoft recommender to reconstruct our code.
Referring from Microsoft recommender, Ruizheng Shen suggests searching on SAR, NCF, BPR and RBM, as there are many online resources that introduce these four algorithms, and relatively simple.
Other algorithms such as DeepRec, NewsRec are using advance neural network structures like LSTM and self-attention, which are difficult to fine-tune and train. As we do not have too much time 
remaining for development, using advanced neural network is too risky.
Hongcong Zhu suggests we could assign each algorithms to one team member, so that the researching could be parallel and high-efficient.

#### Action Points
| Action Items                                                                       | Person Responsible |          Deadline |
|:-----------------------------------------------------------------------------------|:------------------:|------------------:|
| Assign team members to do research on SAR, NCF, BPR and RBM at next weekly meeting |   Ruizheng Shen    |  13rd April, 2022 |
