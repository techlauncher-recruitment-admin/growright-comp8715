# Team Meeting

---
## Meeting Time
- Date: 2022.03.29
- Participants:
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - Learn Berts for those who don't know. 
  - Foucs on the mid-exam and after that we continue to do the project( Maybe do more during the teaching break). 
  - Communicate on time once have problems.

## Meeting Content:

### Topic 1: The rehearsal of Audit 2
- Holder: Xinjia Zhang
- Expect Time: 30 minutes

#### Discussion
The meeting included, among other things, a risk register, an issues register and a task report on the completion of the decision log. As well as a walkthrough of Audit 2. xinjia chaired the meeting, xixiang recorded the meeting and kept the documentation up to date. During the meeting, the team members presented in turn what they had completed in the previous phase, and ruizheng and hongcong suggested the possibility of using the recommendation algorithm and provided valid links for the group to learn from. Based on xixiang's suggestion, it was decided to participate in the PRESENTATION together, with all presenting in turn.
According to chengyu's suggestion, the holiday after mid-term exams was an appropriate time for all of us to use to focus on breaking through the technical barriers of the project. In the next phase, the content was divided into two main areas. On the one hand, technically we needed to test the machine learning models at different stages and build the tagger using Berts; on the other hand, based on xigeng's suggestion, we decided to increase the frequency of meetings but reduce the time of a single meeting to increase efficiency.

#### Action Points
| Action Items                                                                                                                      |     Person Responsible     |          Deadline |
|:----------------------------------------------------------------------------------------------------------------------------------|:--------------------------:|------------------:|
| Improving the visualisation of the homepage                                                                                                          | Xixiang Chen, Xinjia Zhang |  4th April, 2022 |
| Writing test samples                                                                                               | Hongcong Zhu | 5th, April, 2022 |
 | Learn about bert and other related knowledge and conduct research |  Xixiang Chen, Xinjia Zhang  | 4th, April, 2022 |
 | Try writing some demos for testing                                                                                       |        Ruizheng Shen, Xigeng Li, Chengyu Zhang        |  6th April, 2022 |


