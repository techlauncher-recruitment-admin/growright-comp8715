# TechLauncher Project Audit 1 Meeting Report
# Date: 2022.03.09
## Participants:
    Peter (tutor), Andy (tutor), Andrea (examiner)
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
## Meeting Objective:
    Showing the group progress, enhancing all project work through constructive, actionable feedback with critical and considered review.
## Action points from the meeting:
- Alter statement of work to provide more detailed plan.
- Provide detailed descriptions on final deliverables.
- Alter stakeholder analysis.
- Fix landing page access issue for clients.
- Display more evidence of research and work.
- Provide more accurate timeline.