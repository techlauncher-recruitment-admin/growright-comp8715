# Team Meeting

---
## Meeting Time
- Date: 2022.03.01
- Participants:
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - Understand the project content, objectives and discuss how the project will run.
### Topic 1: Understanding the project, aligning objectives, identifying problems and finding measures
- Holder: Xinjia Zhang, Ruizheng Shen
- Expect Time: 147 minutes

#### Discussion
Firstly, xinjia and ruizheng presented an overview of the project, the progress, the approach to the difficulties, and learned about the areas of expertise of each team member to facilitate work allocation. Secondly, based on the overview of the GrowRight and ERICA solutions, hongcong led the group through the question of "what do we need and how can your team help", arguing that we should provide the right trained models to fit the database and front-end interfaces. The group then agreed on the scope of work for the semester based on chengyu's suggestions. xigeng suggested that we should do a two-way recommendation for employees and employers, while xixiang suggested that we should first implement the recommendation of employees to employers, as in the stakeholder analysis, employers have more rights and interests in the project. Next, the group agreed on a project management process and defined the next steps.
#### Action Points
| Action Items                                                                                                                      |     Person Responsible     |          Deadline |
|:----------------------------------------------------------------------------------------------------------------------------------|:--------------------------:|------------------:|
| Detailed understanding of client company needs                                     | Xixiang Chen, Xinjia Zhang |  March 16, 2022 |
| Review of existing test data provided by the client                           | Chengyu Zhang,Xigeng Li | March 18, 2022 |
 | Access to research client database |  Hongcong Zhu | March 20, 2022 |
  | Develop project specific process plans |  Ruizheng Shen  | March 20, 2022 |

