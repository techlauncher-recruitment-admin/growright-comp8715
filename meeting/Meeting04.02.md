# Team Meeting

---
## Meeting Time
- Date: 2022.04.02
- Participants:
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - Improve project documentation.
  - Solve technical constrains on data.

## Meeting Content:

### Topic 1: Documentation Improvement
- Holder: Ruizheng Shen
- Expect Time: 10 minutes

#### Discussion
We identified that our documentation is 'not clear' and 'confusing', by the reflection of feedbacks from tutors, shadows and clients.
Reflecting from tutors feedback, we need to maintain a log to track our efforts, as our product might not be easy to visualize and tutor needs some thing to convince that we are making efforts on it.
Ruizheng learned from other team's output and suggested that the decision log, issue registration and issue registration should have more scope of decisions, and for each scope, we need to have clear definition.
Ruizheng suggests to learn from team Big Dream that we need to define impact level and severity on decision, because we need to know which decision has higher priority. 
Ruizheng also discovers that we need a more specified role assignment.
The impact level should show the expect time cost and the severe level should reveal how emergent the decision need to be discussed.
Xixiang Chen suggests that we need to redesign the landing page to make the documents easier to be found.
Hongcong Zhu finds the data preprocessing code are basically a draft, which does not have appropriate APIs and documentation. He suggests that the documentation needs to be maintained,
so that the code could be re-used more easily. Xixiang suggests that instead of maintaining a documentation for a draft code, we could write a report on the data preprocessing methods, so that when we re-construct
the code, we can have a clear understanding on what need to be done on preprocessing. Ruizheng recommends that the data preprocessing code could be re-construct as methods, so that we can re-use the code through APIs, 
instead of running a script.

#### Action Points
| Action Items                                                                                                                      |     Person Responsible     |          Deadline |
|:----------------------------------------------------------------------------------------------------------------------------------|:--------------------------:|------------------:|
| Rebuild the landing page                                                                                                          | Xixiang Chen, Xinjia Zhang |  10th April, 2022 |
| Write report for data preprocessing                                                                                               | Xixiang Chen, Xinjia Zhang | 10th, April, 2022 |
 | Define impact level and severity level, and rewrite the records in decision log, risk registration log and issue registration log |  Xigeng Li, Chengyu Zhang  | 10th, April, 2022 |
 | Make a deliverable document to show efforts                                                                                       |        Xixiang Chen        |  10th April, 2022 |
 | Remake the role of team members and make a role explanation document                                                              | Xixiang Chen, Xinjia Zhang |  10th April, 2022 |
| Reconstruct the code of data preprocessing                                                                                        | Xixiang Chen, Xinjia Zhang | 10th, April, 2022 |

### Topic 2: Solve Technical Constrains on Data.
- Holder: Hongcong Zhu
- Expect Time: 20 minutes

#### Discussion
Hongcong Zhu discovered that the dataset have no label for training and there are too many empty value in the recommendation item, so that if we just simply delete all the data which contains empty recommendation, we will lose too much data.
Ruizheng Shen suggests that instead of delete the empty values, we replace the empty recommendation with an empty string which contains no characters, which matches the data type of non-empty values, so that when we do preprocessing on data,
we do not need extra step to clean up empty recommendations.
Hongcong Zhu adds that we can first concatenate the feature of the data(skills, work experience, job title, etc) together, then embedding them, instead of only embedding recommendation and description information, so that we can include the 
basic filtering like skills in our model.
Based on these, Ruizheng Shen decides to rewrite the embedding methods of TF-IDF, bert and word2vec.
To deal with the missing label, Ruizheng Shen suggests that we can try to learn the algorithm of recommending system like Youtube. We can consider finding a suitable candidate as recommending a candidate to employer. Ruizheng Shen says he will
have a look on recommendation system, can find some possible open source algorithms.


#### Action point:
| Action Items                             | Person Responsible |          Deadline |
|:-----------------------------------------|:------------------:|------------------:|
| Research on recommendation system        |   Ruizheng Shen    |  10th April, 2022 |
 | Rewrite TF-IDF, BERT, Word2Vec embedding |   Ruizheng Shen    | 17th, April, 2022 |