# Team Meeting

---
## Meeting Time
- Date: 2022.03.24
- Participants:
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - Summarise the problems in audit1 and assign tasks
  - Share the results of their programming in the previous period
  
## Meeting Content:

### Topic 1: make a conclusion about the failure of Audit 1
- Holder: Hongcong Zhu
- Expect Time: 40 minutes

#### Discussion
hongcong led the group in summarising and analysing the issues that arose in audit1. Based on the feedback we received, ruizheng felt that our audit1 lacked sufficient visual outcomes and therefore our progress was difficult to assess. Secondly, xixiang felt that we did not deeply analyse the scoring criteria before audit1, and therefore may have been off in the direction of content design. xinjia felt that we should have maintained more communication with the client to confirm their needs and expectations. xigeng and chengyu suggested that we work more on documentation before audit2. It should make our process look more intuitive.


### Topic 2: report the process of the project
- Holder: Xigeng Li
- Expect Time: 20 minutes

#### Discussion
After concluding the summary, the group shared in turn the results of their programming in the first month. xinjia and xixiang successfully implemented data pre-processing, converting complex Excel tables into csv format and removing values with high missing rates. hongcong and ruizheng implemented tf-idf and some other data conversions. xigeng and chengyu implemented word vector conversion using NLTK and bert-as-services.


#### Action Points
| Action Items                                                                                                                      |     Person Responsible     |          Deadline |
|:----------------------------------------------------------------------------------------------------------------------------------|:--------------------------:|------------------:|
| Changing the format of minutes and refining authority and responsibility                                                                                                         | Xixiang Chen |  5th April, 2022 |
| Explore more word vector conversion methods                                                                                              | Hongcong Zhu | 10th, April, 2022 |
 | Communicate with customers to understand needs and expectations|  Xinjia Zhang  | March 25, 2022 |
 