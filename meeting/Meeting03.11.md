# Team Meeting

---
## Meeting Time
- Date: 2022.03.11
- Participants:
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - Confirm data cleaning content, keyword information extraction, word vectorization tasks, and technical documentation tasks. 
  - Make project task assignments.


## Action points：
### Tasks assignment:
- Deduplication (duplicate ID, removal of excessive missing items, Chinese->English) — Xixiang, Xinjia, Wednesday.
- Extraction: keyword extraction — Ruizheng Hongcong, Thursday.
- Transposerical vector: spacy, bert — Xigeng, Chengyu, Friday.
### Technical documentation:
- Share the technical documents you have compiled to the WeChat group chat
### Data cleaning:
- If the useful information blank rate of a row exceeds 60%, the row is removed.
- Candidate: id, current role, 
- Employment: Job title, company, time employed, (end-date, whether working in this category or not), location(filter). Description(keyword extraction)
- Education: Institute, degree(filter), field of study(filter), 
- skill: skill(filter), rating(pagerank?),
- Interest: Interest list
- Recommendation: Role, When(sort), Relationship(keyword extraction), Recommendation(keyword extraction).
- Employment: Dict(id: Dict(job title: string), Dict(company: string), Dick(time employed: date),…..)
