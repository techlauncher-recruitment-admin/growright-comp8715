# Team Weekly Meeting

---
## Meeting Time

- Date: 2022.04.20
- Participants:
    Ruizheng Shen, Xigeng Li, Xinjia Zhang, Xixiang Chen, Chengyu Zhang, Hongcong Zhu
- Purpose:
  - Finding Out Solution to the Lack of Data

## Meeting Content:

### Topic: Finding out Solution to the Lack of Data
- Holder: Ruizheng Shen
- Expect Time: 15 minutes

#### Discussion
Ruizheng Shen, Xigeng Li, Chengyu Zhang, Hongcong Zhu finished searching for algorithms and identified that
all the algorithms needed extra data. The data should be historical searching data which needed be obtained 
from application backend, containing employer searching records.
Hongcong Zhu suggests that we should communicate with client to see if they have new data. Xixiang mentioned 
that client had said in the first client meeting that the cost of data was too expensive, so they might be unable
to obtain new data. Ruizheng Shen suggests showing the result of our researches to client and ask for suggestions,
as we have already had a workable model which could select the top scoring candidate from candidates list.
We decide to arrange a meeting with client in April 22nd. While in the meeting, we show our researches one-by-one to our
client and discuss what kinds of data we need for the algorithms.

#### Action Points
| Action Items                                            |                  Person Responsible                   |         Deadline |
|:--------------------------------------------------------|:-----------------------------------------------------:|-----------------:|
| Email client to arrange a meeting(With agenda and date) |                     Ruizheng Shen                     | 20th April, 2022 |
| Prepare for presenting research results                 | Ruizheng Shen, Xigeng Li, Hongcong Zhu, Chengyu Zhang | 21st April, 2022 |

#### Addition
Our planning for client meeting:
1.  Introduce the system architecture, introduce the group technical processing flow.
    and consult whether the selected system architecture is reasonable by Ruizheng
    1. Overview of system architecture
    2. Processing flow of data
    3. Whether the selected system is reasonable

2. Report related methods of data preprocessing by Hongcong 
3. Report TF-IDF technology and word2vec technology by Ruizheng
    1. Word embedding
    2. Tf-idf and reason
    3.  word2vec and reason   
4. Report related searches for the sorting layer.
    1. SAR Technology : xigeng
    2. RBM Technology ：chengdu
    3. BPR Technology ： Hongcong
    4.  NCF Technology ：RuiZheng
    5.  Decision Trees and Random Forests
    6. Missing data
5. Suggestions for document
    1. Possible output
    2. Decision log present
    3. Ask suggestions of our documentation.