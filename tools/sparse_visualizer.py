# coding=utf-8
import matplotlib.pyplot as plt


class SparseVisualizer:
    def __init__(self, sparse_matrix=None):
        self._sparse_matrix = sparse_matrix  # Store the sparse matrix(csr_matrix) that need to be visualized.
        self._dense_matrix = None

    @property
    def sparse_matrix(self):
        if self._sparse_matrix is None:
            raise Exception("Sparse matrix has not been initialized!")
        else:
            return self._sparse_matrix

    @property
    def dense_matrix(self):
        if self._sparse_matrix is None:
            raise Exception("Sparse matrix has not been initialized!")
        else:
            self._dense_matrix = self._sparse_matrix.todense()
            return self._dense_matrix

    @sparse_matrix.setter
    def sparse_matrix(self, s):
        self._sparse_matrix = s

    def visualize_sparse_matrix(self):
        if self._sparse_matrix is None:
            raise Exception("Sparse matrix has not been initialized!")
        else:
            plt.title('Sparse Matrix Visualize')
            plt.tight_layout()
            plt.spy(self._sparse_matrix, markersize=1)
            plt.show()
