import pandas as pd
import numpy as np

'''
For python3.7, you need to install a package called openpyxl which exits in python3.8 or higher but not python3.7, to 
open .xlsx file.
You can install it in Preference->Project:...->Python interpreter->"+"to add package openpyxl.
'''

def is_ascii(s):
    '''
    determine that all characters of String s are the worlds and the symbols in English
    This method aim to find the unusual characters in String s.
    :param s: Any string
    :return: Boolean
    '''
    return all(ord(c) < 128 for c in s)


def replaceToNull(df):
    '''
    To replace any string containing unusual character to None in data frame df.

    :param df:A data frame
    :return:A data frame without unusual characters.
    '''
    row, col = df.shape
    for i in range(row):
        for j in range(col):
            cur_value = df.iloc[i, j]
            cur_value = str(cur_value)
            if is_ascii(cur_value) == False:
                df.iloc[i, j] = None



def dataCleaning():
    '''
    Replace any string which contains unusual characters by None.
    Delete duplicated row.
    Delete high blank rate row.
    Concat sheets into one sheet.
    :return: a data frame
    '''
    # Candidate data cleaning
    data = pd.read_excel("Data.xlsx", "Candidates")
    replaceToNull(data)
    data = data.dropna()
    data.reset_index(drop=True, inplace=True)
    data = data.drop_duplicates('ID', keep='first')

    # Employment data cleaning
    data1 = pd.read_excel("Data.xlsx", "C-Employment")
    replaceToNull(data1)
    data1 = data1.loc[data1.isnull().mean(axis=1) < 0.6]
    data1 = data1.drop_duplicates('ID', keep='first')
    data1.reset_index(drop=True, inplace=True)

    # Education data cleaning
    data2 = pd.read_excel("Data.xlsx", "C-Education")
    replaceToNull(data2)
    data2 = data2.loc[data2.isnull().mean(axis=1) < 0.7]
    data2 = data2.drop_duplicates('ID', keep='first')
    data2.reset_index(drop=True, inplace=True)

    # Skills data cleaning
    data3 = pd.read_excel("Data.xlsx", "C-Skills")
    replaceToNull(data3)
    data3['Rating'] = data3['Rating'].fillna(0) # fill 0 for any blank space in Rating column.

    #  Interest data cleaning
    data4 = pd.read_excel("Data.xlsx", "C-Interests")
    replaceToNull(data4)
    data4 = data4.loc[data4.isnull().mean(axis=1) < 0.7]
    data4 = data4.drop_duplicates('ID', keep='first')
    data4.reset_index(drop=True, inplace=True)

    # Concat these different data frames into one data frame according to matching their ID.
    # And skills sheet have different rows for one ID, so we leave it alone.
    dataA = data
    dataB = data1
    dataC = data2
    dataD = data4
    dataFinal1 = dataA.merge(dataB, how="outer", on='ID')
    dataFinal2 = dataFinal1.merge(dataC, how='outer', on='ID')
    dataFinal3 = dataFinal2.merge(dataD, how='outer', on='ID')
    dataFinal = dataFinal3.loc[dataFinal3.isnull().mean(axis=1) < 0.7]
    dataFinal = dataFinal.drop_duplicates('ID', keep='first')
    dataFinal.reset_index(drop=True, inplace=True)

    return dataFinal