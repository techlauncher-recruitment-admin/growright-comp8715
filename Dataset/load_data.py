# coding=utf-8
"""
@author: Ruizheng Shen
load_data.py:
    Load dataset, do data processing and cleaning.
    Note: It is a draft of converting recommendations to tf-idf sparse matrix. Currently, I am working on reformat these
    codes to OOD style.
"""

import numpy as np
import pandas as pd
import logging
from sklearn.feature_extraction.text import TfidfVectorizer
from Playground.tf_idf_converter import TfIdfConverter
from tools.sparse_visualizer import SparseVisualizer


recommendation_data = pd.read_excel('../data/recommadation_final.xlsx', 'Sheet1')  # Read preprocessed recommendation sheet
number_of_data = recommendation_data.shape[0]
feature_names = ['Recommendation_1', 'Recommendation_2', 'Recommendation_3']
print(recommendation_data[feature_names].loc[0][1])
'''
Drop N/A value in these recommendation columns separately, translate these columns to numpy array and join them into 
one array.
'''
corpus = None
for feature in feature_names:
    column = recommendation_data.loc[:, feature]
    column = column.dropna()
    if corpus is None:
        corpus = column.to_numpy()
    else:
        column = column.to_numpy()
        corpus = np.concatenate((corpus, column))

'''
Build tf-idf vectorizer and translate the document into sparse matrix.
'''
converter = TfIdfConverter()
converter.build_converter(corpus)
recommendation_data = recommendation_data.loc[:, feature_names].to_numpy()
for i, recom_list in enumerate(recommendation_data):
    for j, recom in enumerate(recom_list):
        if not pd.isna(recom):
            recommendation_data[i][j] = converter.convert_text([recom])

print(recommendation_data[0][1])
print(recommendation_data.shape)

sv = SparseVisualizer(recommendation_data[0][1])
sv.visualize_sparse_matrix()

'''
example = ['Current meeting']
tf_idf_vectorizer = TfidfVectorizer()
sparse_matrix = tf_idf_vectorizer.fit_transform(example)
'''
