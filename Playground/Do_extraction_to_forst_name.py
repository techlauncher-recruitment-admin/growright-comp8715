import pandas as pd
import spacy
from triples_from_text import extract_triples
nlp = spacy.load("en_core_web_sm")
def do_extraction(text):
    triples = extract_triples(text)
    out_list = []
    for element_list in triples:
        if element[1] in element_list:
            for word in element_list:
                if word != element[1]:
                    out_list.append(word)
    word =''
    for value in out_list:
        word +=value +' '
    return word


if __name__ == "__main__":
    data = pd.read_csv('testcsv_add_firstName.csv')
    data.drop(columns=['Index', 'Unnamed: 0'],inplace=True)
    df_li = data.values.tolist()
    out =[]
    for element in df_li:
        if str(element[1]) !='nan':
            text = str(element[6])
            element[6]=do_extraction(text)
            if str(element[11] !='nan'):
                text = str(element[11])
                element[11] = do_extraction(text)
            if str(element[16] !='nan'):
                text = str(element[16])
                element[16] = do_extraction(text)
            out.append(element)
        else:
            out.append(element)
    print(out)
    max = 0
    heand_list = []
    heand_list.append('ID')
    heand_list.append('First Name')
    form_header = ['Who', 'Role', 'When', 'Relationship', 'Recommendation']
    n = 1
    while (n < 4):
        for element in form_header:
            heand_list.append(element + '_' + str(n))
        n += 1
    test = pd.DataFrame(columns=heand_list, data=out)
    out_string = 'testcsv_final.csv'
    test.to_csv(out_string, encoding='UTF-8')
    print("Finished the process.")
    #data.to_csv('last_step_extraction.csv')

