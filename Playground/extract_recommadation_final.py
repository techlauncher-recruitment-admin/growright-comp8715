import pandas as pd
from triples_from_text import extract_triples
def do_extraction(url,dict_1):
    df = pd.read_csv(url, usecols=[1], names=None)  # 读取项目名称列,不要列名
    df_1 = pd.read_csv(url, names=None)
    de_li1=df_1.values.tolist()
    df_li = df.values.tolist()
    symbol = {}
    out_list=[]
    for i in range(0,len(df_li)):
        element =de_li1[i][1:len(de_li1[i])]
        list_1 =df_li[i][0].split('/')
        if len(list_1) >=5:
            str_1 =""
            for i in range(0,5):
                str_1+=list_1[i]+"/"
            if 'NA' in str_1:
                str_1_list =str_1.split("NA")
                str_1=str_1_list[0]+'n/'+"a"+str_1_list[1][:-1]
                print(str_1)
            element[0] =str_1
            if symbol.get(str_1) == None:
                name = dict_1.get(str(str_1))
                element[1] =name
        out_list.append(element)
    heand_list = []
    heand_list.append('URL')
    heand_list.append('Name')
    form_header = ['Who', 'Role', 'When', 'Relationship', 'Recommendation']
    n = 1
    while (n < 4):
        for element in form_header:
            heand_list.append(element + '_' + str(n))
        n += 1
    test = pd.DataFrame(columns=heand_list, data=out_list)
    test.to_excel('recommadation.xlsx',encoding='UTF-8',index =False)






def excel_search_dict(url):
    df = pd.read_excel(io=url, usecols=[0,1,2], names=None)  # 读取项目名称列,不要列名
    df_li = df.values.tolist()
    symbol = {}
    for s_li in df_li:
        if symbol.get(s_li[0]) == None:
            symbol.update({str(s_li[0]): str(s_li[1])+" "+str(s_li[2])})
    return symbol
def do_data_clean(url):
    df_1 = pd.read_excel(io=url, usecols=[6, 11, 16], names=None)
    df_li = df_1.values.tolist()
    df_2 = pd.read_excel(io=url, usecols=[1], names=None)
    df_li2 = df_2.values.tolist()
    df_3 = pd.read_excel(io=url, names=None)
    df_li3 = df_3.values.tolist()
    out_list1 =[]
    for i in range(0,len(df_li)):
        element =df_li[i]
        init_index = df_li3[i]
        if str(df_li2[i][0]!="nan"):
            name_out =str(df_li2[i][0].split(" ")[0])
            print(name_out)
        for m in range(0,len(element)):
            if str(element[m]) !='nan':
                out_list =extract_triples(str(element[m]))
                str_out =""
                for senetnce in out_list:
                    if name_out in senetnce:
                        for word in senetnce:
                            if word != name_out:
                                str_out+=word+" "
                init_index[m*5+6]=str_out
        out_list1.append(init_index)
    heand_list = []
    heand_list.append('URL')
    heand_list.append('Name')
    form_header = ['Who', 'Role', 'When', 'Relationship', 'Recommendation']
    n = 1
    while (n < 4):
        for element in form_header:
            heand_list.append(element + '_' + str(n))
        n += 1
    test = pd.DataFrame(columns=heand_list, data=out_list1)
    test.to_excel('recommadation_final.xlsx', encoding='UTF-8', index=False)


if __name__ == "__main__":
    # dict = excel_search_dict('Id_name.xlsx')
    # do_extraction('last_step_extraction.csv',dict)
    do_data_clean('recommadation.xlsx')


#https://www.linkedin.com/in/germaine-symons-546b2853/