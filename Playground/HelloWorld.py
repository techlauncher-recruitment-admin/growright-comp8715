import numpy as np

def test_package():
    return np.zeros((10, 2))


if __name__ == '__main__':
    print("Hello, testing numpy package here.")
    np_array = test_package()
    print("should be a 10 * 2 zero matrix")
    print(np_array.shape)
