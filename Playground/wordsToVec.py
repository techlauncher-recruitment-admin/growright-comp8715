# coding=utf-8
"""
@author: Xigeng Li
wordsToVec.py:
    This file's methods are to tokenize text into words and then apply bert(https://github.com/google-research/bert)
    to turn each words into a vector.
"""

import sys
import xlrd
from openpyxl import load_workbook
from bert_serving.client import BertClient
import sys
import xlrd
from numpy import *
import numpy as np
import csv
import nltk
from nltk.corpus import stopwords
import re
from nltk import word_tokenize

#text = "Australia news LIVE: China sanctions to be backed by Australia if arms sent to Russia as Ukraine war continues; attacks on Kyiv intensify; Australian fuel prices soar"


def token(text):
    """
    For a section of text, convert it into sequence of words.
    :param text: input text(paragraph) -- string
    :return: list of words -- list
    """
    sentence = nltk.sent_tokenize(text)  # Separate the paragraph into sentences.
    words = []
    for sent in sentence:
        words.append(nltk.word_tokenize(sent))

    stop = set(stopwords.words('english')) #去除停用词
    filter_text = re.sub(r'[^a-zA-Z0-9\s]','',string= text)
    filter_text = [word for word in filter_text.split(' ') if word not in stop] #去除标点
    # print(filter_text)
    return filter_text


# 传入一个Excel，输出如下数据结构，{第一行第一列：[第一行第二列，....，第一行第n列]，.....第m行第一列：[第m行第二列，....，第m行第n列]}
def mkList(excel):
    allDict = {}
    excel = xlrd.open_workbook(excel)  # 打开excel文件
    sheet = excel.sheet_by_index(0)  # 根据下标获取工作薄
    rowNum = sheet.nrows #获取工作薄数据总行数
    cloNum = sheet.ncols #获取工作薄数据总列数
    print(rowNum,cloNum)
    for i in range(0,rowNum):
        content = []
        for j in range(1,cloNum):
            # word = token(sheet.row_values(i)[j])
            word = sheet.row_values(i)[j]
            content.append(word)
        allDict['{}'.format(sheet.row_values(i)[0])] = content

    return allDict
    # sheet1=excel.sheet_by_name('Sheet1')#根据工作薄名称获取工作薄
    # print(excel.sheet_names()) #获取所有工作薄名称 如['Sheet1', 'Sheet2', 'Sheet3']
    # print(sheet.name) #获取工作薄名称（比较少用）
    # 数据操作
    # print(sheet.col_values(0))  # 获取第一列数据
    # print(sheet.row_values(0)) #获取第一行数据
    # print(sheet.cell(0,0).value) #获取第一行第一列单元格的数据


# 执行bert,输入一个长度为n的list，返回 n 乘 768 纬的矩阵
def getWordVector(wordList):
    bc = BertClient(ip="localhost", check_version=False, check_length=False)
    try:
        vec = bc.encode(wordList)
        print(vec.shape)
        return vec
    except:
        print('空的')
        return


# 调用bert def，将词换成词向量，数据结构不变
def dicToVec(dataList):
    newDic = dataList
    for k in dataList.keys():
        vecBack = getWordVector(dataList[k])
        newDic[k] = vecBack
    print('新字典生成')
    return newDic



def saveUnivData(f,dict):
    writer = csv.writer(f)
    title = []
    for k in dict.keys():
        title.append((k,dict[k]))
    writer.writerows(title)
    # 将列表的每条数据依次写入csv文件， 并以逗号分隔
    print("写入完成....")


if __name__ == '__main__':
    np.set_printoptions(threshold=np.inf)
    getList = mkList('./{}'.format(sys.argv[1]))
    print(getList)
    newDic = dicToVec(getList)
    output = open(sys.argv[2], 'w')
    saveUnivData(output, newDic)
    # table = excel.get_sheet_by_name('Sheet1')
    # rows = table.max_row  # 获取行数
    # cols = table.max_column  # 获取列数
    # print(rows,cols)
    # row = table.rows
    # for row in ws.iter_rows('A1:D4'):
    #     for cell in row:
    #         print(cell.value)







