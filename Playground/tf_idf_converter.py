# coding=utf-8
"""
@author: Ruizheng Shen
tf_idf_converter.py:
    Method for creating tf-idf embedding for words.
    License:
        - Sklearn, link: https://github.com/scikit-learn/scikit-learn
    Note:
        - Consider self-implement tf-idf vectorizer.
"""
from sklearn.feature_extraction.text import TfidfVectorizer


class TfIdfConverter:
    def __init__(self):
        self.converter = TfidfVectorizer()
        self.word_vector_map = {}
        self.is_fit = False

    def build_converter(self, corpus):
        """
         - Given a corpus(which is a list where each element is text/paragraph/sentence), build up vector representation
        for each word appears in the corpus(using tf-idf).
         - Same words will have same vectors.
         - To avoid bias on synonym, recommend stemming and lemmatizing the corpus before applying tf-idf converter.

        :param corpus: list of text -- list
        :return: built vectorizer
        """
        X = self.converter.fit_transform(corpus)
        self.is_fit = True
        self.word_vector_map = self.converter.vocabulary_
        return X

    def convert_text(self, text):
        """
        For input text, return a sparse tf-idf matrix.
        :param text: raw text(Recommend stemming and lemmatizing before transforming)
        :return: tf-idf matrix
        """
        if not self.is_fit:
            return None
        return self.converter.transform(text)