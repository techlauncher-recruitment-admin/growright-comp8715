#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import pandas as pd
import csv

from triples_from_text import extract_triples
from collections import Counter   #引入Counter
# def process_all():
#     while(True):
#         text = input("input a text:")
#         triples = extract_triples(text)
#         print("\n\n===============the result=============\n\n")
#         print(triples)
#         for index in triples:
#             for element in index:
#                 if element =='Steve':
#                     print(index)

# Reads data file and creates the submission.csv file
if __name__ == "__main__":
    data = pd.read_csv('testcsv.csv')
    out_list =[]
    df_li =data.values.tolist()
    for element in df_li:
        if str(element[2]) =='nan':
            text =str(element[7])
            triples = extract_triples(text)
            first_name_list =[]
            for element_list in triples:
                for element_last in element_list:
                    substring =" "
                    if substring not in element_last:
                        if ord(element_last[0]) in range(65,91):
                            first_name_list.append(element_last)
            if len(first_name_list)==1:
                element[2] = str(first_name_list[0])
                out_list.append(element)
            else:
                b = dict(Counter(first_name_list))
                max =0
                for key in b.keys():
                    if b.get(key) >max:
                        max =b.get(key)
                for key in b.keys():
                    if b.get(key) == max:
                        element[2] = str(key)
                out_list.append(element)
        else:
            out_list.append(element)
    max = 0
    heand_list = []
    heand_list.append('Index')
    heand_list.append('ID')
    heand_list.append('First Name')
    form_header = ['Who', 'Role', 'When', 'Relationship', 'Recommendation']
    n = 1
    while (n < 4):
        for element in form_header:
            heand_list.append(element + '_' + str(n))
        n += 1
    test = pd.DataFrame(columns=heand_list, data=out_list)
    out_string = 'testcsv_add_firstName.csv'
    test.to_csv(out_string, encoding='UTF-8')
    print("Finished the process.")

