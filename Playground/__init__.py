#!/usr/bin/python/2022
# -*- coding: utf-8 -*-
import time
import numpy as np
from sklearn.preprocessing import MinMaxScaler


def hello_gr2022(your_name):
    name_vector = np.zeros(len(your_name))
    for i, ch in enumerate(your_name):
        name_vector[i] = ord(ch)
    name_diag = np.diag(name_vector)
    return name_diag


def singing_gr2022(your_name):
    # harmonic_list = np.array([65.41, 98.00, 130.81, 164.81, 196.00, 220.00, 246.94, 261.63, 293.66, 329.63, 369.99, 392.00, 440.00, 493.88, 739.99, 880.00, 1108.73, 1396.91, 1567.98, 1975.53, 2637.02])
    harmonic_ratio = np.asarray([1.0, 3/2, 5/4])
    sps = 44100
    standard_freq_hz = 440
    duration_s = 2.0
    atten = 0.3
    each_sample_num = np.arange(duration_s * sps)
    waveform = np.sin(2 * np.pi * each_sample_num * standard_freq_hz / sps)
    ratio_vector = np.zeros(len(your_name) - 1)
    for i in range(len(your_name) - 1):
        current_ratio = ord(your_name[i + 1]) / ord(your_name[i])
        print(current_ratio)
        idx = (np.abs(harmonic_ratio - current_ratio)).argmin()
        ratio_vector[i] = harmonic_ratio[idx] * np.random.choice([1, 2])
    print(ratio_vector)
    for ratio in ratio_vector:
        waveform += np.sin(2 * np.pi * each_sample_num * (ratio * standard_freq_hz) / sps)
    waveform_quiet = waveform * atten
    sd.play(waveform_quiet, sps)
    time.sleep(duration_s)
    sd.stop()



if __name__ == "__main__":
    print("Welcome to GR2022 team!")
    name = input("Please input your name here for fun: ")
    print("Your name will look like this in diag matrix: ")
    print(hello_gr2022(name))
    print("Or, if you wanna know what your name vector sound like?")
    con = input("Press any key to continue(might be noisy and annoying... Be careful to continue)")
    singing_gr2022(name)
