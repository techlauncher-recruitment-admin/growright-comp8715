import pandas as pd

def excel_search_dict(url):
    df = pd.read_excel(io=url, usecols=[0,1], names=None)  # 读取项目名称列,不要列名
    df_li = df.values.tolist()
    symbol = {}
    for s_li in df_li:
        if symbol.get(s_li[0]) == None:
            symbol.update({str(s_li[0]): str(s_li[1])})
    return symbol

def excel_final_dict(url,dict_1):
    df = pd.read_excel(io=url, usecols=[1], names=None)  # 读取项目名称列,不要列名
    df_li = df.values.tolist()
    symbol = {}
    for s_li in df_li:
        if symbol.get(s_li[0]) == None:
            name =dict_1.get(str(s_li[0]))
            symbol.update({str(s_li[0]):name})
    return symbol

def change_csv(url,dict):
    df =pd.read_excel(io=url)
    df_li =df.values.tolist()
    out_put_list =[]
    for s_li in df_li:
        if str(s_li[0])=='nan':
            list1 =out_put_list[len(out_put_list)-1]
            for i in range(1,len(s_li)):
                list1.append(s_li[i])
        else:
            out_put_list.append(s_li)
    max =0
    heand_list =[]
    heand_list.append('ID')
    heand_list.append('First Name')
    form_header = ['Who','Role','When','Relationship','Recommendation']
    n=1
    while(n<4):
        for element in form_header:
            heand_list.append(element+'_'+str(n))
        n+=1

    for i in range(0,len(out_put_list)):
        out_put_list[i].insert(1,dict.get(out_put_list[i][0]))
        if len(out_put_list[i])>17:
            list_out= out_put_list[i][:17]
            out_put_list[i]=list_out
    test = pd.DataFrame(columns=heand_list, data=out_put_list)
    df_li = test.values.tolist()
    out_list =[]
    for index in range(0,len(df_li)):
        if str(df_li[index][2])=='nan':
            out_list.append(index)
    print(out_list)
    test.drop(test.index[out_list], inplace=True)
    out_string = 'testcsv.csv'
    test.to_csv(out_string, encoding='UTF-8')

if __name__ == "__main__":
    dict=excel_search_dict('Id_name.xlsx')
    change_csv('recommadation.xlsx',dict)
    print("Finished the process.")
#https://www.linkedin.com/in/germaine-symons-546b2853/