# coding=utf-8
"""
@author: Ruizheng Shen
word_to_vec.py:
    Methods for embedding words using pytorch and bert.
    License:
        - pytorch, link: https://pytorch.org
        - transformers, link: https://github.com/huggingface/transformers
"""

import torch
from transformers import BertTokenizer, BertModel, BertForMaskedLM

# Load pre-trained tokenizer.[script]
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
model = BertModel.from_pretrained('bert-base-uncased')
text = "Australia news LIVE: China sanctions to be backed by Australia if arms sent to Russia as Ukraine war continues; attacks on Kyiv intensify; Australian fuel prices soar"
encoded_input = tokenizer(text, return_tensors='pt')
output = model(**encoded_input)


class WordEmbedding:
    def __init__(self):
        """
        Initialize parameter, using pretrained tokenizer(BertTokenizer) and
        pretrained model(BertModel) from transformers package.
        """
        self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
        self.model = BertModel.from_pretrained('bert-base-uncased')

    def embed(self, _text):
        """
        embedding text(could be either paragraph or sentence).
        :param _text: text --string
        :return: embedded text -- tensor
        """
        encoded_text = self.tokenizer(_text, return_tensors='pt')
        embedded_text = self.model(**encoded_text)
        return embedded_text
