# Team GR 2022

---
Welcome to the code repository!

This is **not** the landing page of our project.

We have moved our landing page to [**confluence**](https://techlauncher-recruitment.atlassian.net/l/c/xnmha1Qw).

Our new Kanban board is in [**Trello**](https://trello.com/b/kQvwrgfr/recruitment-kanban)



---
## Team Charter

- [Team charter](https://anu365-my.sharepoint.com/:w:/g/personal/u7269234_anu_edu_au/EVB1DG31rtRNnw8CdA4YBRgBabYmIRl8-wMnhhZ_wIeQYg?rtime=uAqJ2wMA2kg)

---
## Meeting

- [Meeting](https://bitbucket.org/techlauncher-recruitment-admin/growright-comp8715/src/master/meeting/)

---
## Project Document

 - All the documents are already organized in [Confluence](https://techlauncher-recruitment.atlassian.net/l/c/xnmha1Qw).
 
- [Statement of Work](https://anu365-my.sharepoint.com/:w:/g/personal/u7269234_anu_edu_au/ESUOZyMwzG9Ir-AmUyr9Kv8BpYTeDoyOf3tvmi_mnFxPxQ?e=XkIA9o)
- [Kanban Board](https://techlauncher-recruitment.atlassian.net/jira/software/projects/TR/boards/1/roadmap)
- [Stakeholder Analysis](https://anu365-my.sharepoint.com/:w:/g/personal/u6814863_anu_edu_au/EW4v4Het0gJAqWMkq7dVTskBDcmCbFdmpe4KgWaio3XRwg?e=BZ7zJA)
- [Decision Log](https://anu365-my.sharepoint.com/:x:/g/personal/u7212032_anu_edu_au/ESyiwC-EYd1HuMzanet_wdgBumK4VPy5RZiQuC9NlD80QA?e=piEHzy)
- [Feedback Log](https://anu365-my.sharepoint.com/:x:/g/personal/u7212032_anu_edu_au/EQrTmKUgiipEvrZ5f9VjVtIBqh6SUzo4uvdy0AszTev8Pw?e=637LYJ)
- [Issue Register](https://anu365-my.sharepoint.com/:x:/g/personal/u7212032_anu_edu_au/EXxQrVMuXdhNrEeA-m1BB5wBFXRdohD9p5P_kZ20yej9eQ?e=V09Bfq)
- [Risk Register](https://anu365-my.sharepoint.com/:x:/g/personal/u7212032_anu_edu_au/EQBvcpBrAdBBnF2Ydzc3wLoBkr7AX8Iyx_lKqxd63Rr-5Q?e=JutamK)
- [Trello Board](https://trello.com/b/Fg5bVxPy/gr2022)
- [Backlog](https://trello.com/b/Mlc121sO/backlog)

---
## Technical Context

- [Research on text matching and semantic matching](https://anu365-my.sharepoint.com/:w:/g/personal/u7267141_anu_edu_au/EV8xz__4HTZBrjzLS_0T_34BJFk1TudkZxPNU85_hJiwWA?e=UV7Yl5)
- [Research on TF](https://anu365-my.sharepoint.com/:w:/g/personal/u7267141_anu_edu_au/EQq5mtRFygtMpIZAHOke-w8B3BWya5pO-_VwLSQNdpJ3dQ?e=pHeQVm)
- [Research on Vocabulary-to-word vector](https://anu365-my.sharepoint.com/:w:/g/personal/u7212032_anu_edu_au/ERsECFrBc7VDoyKtpzoscb0BxIGeqFfHXv1AsrnsoRcsCQ?e=94FjTy)

---
## Timeline

- [Timeline](Timeline.pdf)

---
## Other links:
- [COMP8715 piazza forum](https://piazza.com/class/kzklvatqokt5s0?cid=62)
- [Slack](https://app.slack.com/client/TEBDYDMGX/CEBDYDRAT)

---
## Developing Environment(Keep updating packages)
- pycharm recommended(Community is free to use)
- python version 3.6+
- anaconda
- numpy 1.41, [installation guide](https://numpy.org/install/)
- pandas 1.22.0, [installation guide](https://pandas.pydata.org/getting_started.html)
- matplotlib 3.5.1, [installation guide](https://matplotlib.org/stable/index.html)
- scikit-learn, [installation guide](https://scikit-learn.org/stable/install.html)
- pytorch, [installation guide](https://anaconda.org/pytorch/pytorch)
- Hugging face, transformers, [pip installation guide](https://huggingface.co/docs/transformers/installation), [conda installtion guide](https://anaconda.org/conda-forge/transformers)
- BERT, [github website](https://github.com/google-research/bert)
