# coding=utf-8
"""
@author: Ruizheng Shen
word2vec.py:
    Use a pretrained model of word2vec to transfer each word into a numerical vector.
    For the searching keywords, first transfer the keywords into word2vec vector and calculate the cosine similarity
    between keywords and candidates.
    Reference: None
    License: gensim: https://github.com/RaRe-Technologies/gensim-data
    Note: Some methods like data cleaning can be moved here.
"""

import gensim.downloader as api
import nltk.tokenize
from transformers import BertTokenizer
from sklearn.metrics.pairwise import linear_kernel
from sklearn.metrics.pairwise import cosine_similarity

import numpy as np
from nltk.tokenize import word_tokenize


class Word2VecRecommender:
    def __init__(self, pretrained_model='glove-twitter-25', tokenized_method='nltk'):
        self.word2vec_model = api.load(pretrained_model)
        self.tokenizer = None  # Will be stored if using 'bert' tokenizer
        self.tokenized_method = tokenized_method
        self.word2vec_matrix = None
        self.tokens = set()

    def clean_dataframe(self, df, cols_to_clean, new_col_name='cleaned_text'):
        df = df.replace(np.nan, "")
        df[new_col_name] = df[cols_to_clean].apply(lambda cols: " ".join(cols), axis=1)
        # TODO: Apply cleanning data process here.
        return df

    def tokenize_text(self, df_clean, text_col='cleaned_text'):
        columns = df_clean[text_col]
        column_tokenized = None  # Need to be initialized and feed in word vectors
        if self.tokenized_method == 'nltk':
            column_tokenized = columns.copy()
            for i in range(0, len(columns)):
                token_list = word_tokenize(columns[i], language='english')
                vector_of_sentence = np.zeros((self.word2vec_model.vector_size, ))
                for token in token_list:
                    self.tokens.add(token)  # Add new tokens to the token set.
                    if token in self.word2vec_model.key_to_index.keys():
                        word_vec = self.word2vec_model.get_vector(token, norm=True)
                    else:
                        word_vec = np.zeros((self.word2vec_model.vector_size, ))
                    if vector_of_sentence is None:
                        vector_of_sentence = word_vec
                    else:
                        vector_of_sentence = vector_of_sentence + word_vec
                if len(token_list) > 0:
                    vector_of_sentence = vector_of_sentence / len(token_list)
                column_tokenized[i] = vector_of_sentence
            self.word2vec_matrix = column_tokenized
        elif self.tokenized_method == 'bert':
            tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
            column_tokenized = columns.copy()
            for i in range(0, len(columns)):
                token_list = tokenizer.tokenize(columns[i])
                vector_of_sentence = np.zeros((self.word2vec_model.vector_size,))
                for token in token_list:
                    self.tokens.add(token)  # Add new tokens to the token set.
                    if token in self.word2vec_model.key_to_index.keys():
                        word_vec = self.word2vec_model.get_vector(token, norm=True)
                    else:
                        word_vec = np.zeros((self.word2vec_model.vector_size,))
                    if vector_of_sentence is None:
                        vector_of_sentence = word_vec
                    else:
                        vector_of_sentence = vector_of_sentence + word_vec
                vector_of_sentence = vector_of_sentence / len(token_list)
                column_tokenized[i] = vector_of_sentence
            self.tokenizer = tokenizer
            self.word2vec_matrix = column_tokenized
        return column_tokenized

    def cos_similarity(self, keywords, k=5):
        keyword_vec = None
        if self.tokenized_method == 'nltk':
            token_list = word_tokenize(keywords, language='english')
            for token in token_list:
                if keyword_vec is None:
                    keyword_vec = self.word2vec_model[token]
                else:
                    keyword_vec = keyword_vec + self.word2vec_model[token]
            keyword_vec = keyword_vec / len(token_list)
        elif self.tokenized_method == 'bert':
            if self.tokenizer is not None:
                token_list = self.tokenizer.tokenize(keywords)
                for token in token_list:
                    if keyword_vec is None:
                        keyword_vec = self.word2vec_model[token]
                    else:
                        keyword_vec = keyword_vec + self.word2vec_model[token]
                keyword_vec = keyword_vec / len(token_list)
            else:
                raise Exception('The bert tokenizer has not been initialized!')
        cos_similarities = np.zeros(shape=self.word2vec_matrix.shape, dtype=np.float32)
        # cos_similarities = cosine_similarity(keyword_vec.reshape(1, -1), self.word2vec_matrix)
        for i in range(len(cos_similarities)):
            cos_similarities[i] = cosine_similarity(keyword_vec.reshape(1, -1), self.word2vec_matrix[i].reshape(1, -1))[0][0]
        # cos_similarity = linear_kernel(keyword_vec.reshape(-1, 1), self.word2vec_matrix)
        # print(cos_similarities.shape)
        sorted_idx = np.argsort(cos_similarities)  # From small to large
        if len(sorted_idx) < 5:
            return sorted_idx
        else:
            return sorted_idx[-5:]
