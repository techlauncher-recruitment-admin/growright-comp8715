# coding=utf8
from Models.word2vec import Word2VecRecommender
import pandas as pd
import numpy as np


def test_on_feature_data(keyword):
    recommendation_data = pd.read_excel('../../data/recommadation_final.xlsx', 'Sheet1')
    feature_names = ['Recommendation_1', 'Recommendation_2', 'Recommendation_3']
    recommender = Word2VecRecommender()
    cleaned_column = 'recommendation_clean'
    df_clean = recommender.clean_dataframe(recommendation_data, feature_names, cleaned_column)
    tokenized = recommender.tokenize_text(df_clean=df_clean, text_col=cleaned_column)
    ids = recommender.cos_similarity(keyword)
    print('For Keyword: {}, choose following candidates:'.format(keyword))
    for id in ids:
        print('Candidate_id: ', id)
        print(' ', recommendation_data.iloc[id, :])
        input('Continue?')


if __name__ == '__main__':
    keywords = ['software leadership', 'java', 'senior accountant']
    for keyword in keywords:
        test_on_feature_data(keyword)
    # test_on_feature_data()
