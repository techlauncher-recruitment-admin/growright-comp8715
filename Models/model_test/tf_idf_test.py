# coding=utf-8


from Models.tf_idf import TfidfRecommender
from tools.sparse_visualizer import SparseVisualizer
import pandas as pd
import numpy as np


def test_on_feature_data():
    recommendation_data = pd.read_excel('../../data/recommadation_final.xlsx', 'Sheet1')
    print(recommendation_data.loc[5686, :])
    number_of_data = recommendation_data.shape[0]
    feature_names = ['Recommendation_1', 'Recommendation_2', 'Recommendation_3']
    recommender = TfidfRecommender('URL')  # Set URL as the id columnn
    cleaned_column = 'recommendation_clean'
    df_clean = recommender.clean_dataframe(recommendation_data, feature_names, cleaned_column)
    tf, vec_tokenized = recommender.tokenize_text(df_clean, text_col=cleaned_column)
    recommender.fit(tf, vec_tokenized)
    print(recommender.tf_idf_matrix.shape)
    print(recommender.cos_similarity(['pleasure']))
    '''
    sparse_matrix = recommender.tf_idf_matrix
    vs = SparseVisualizer(sparse_matrix)
    print(vs.sparse_matrix[0])
    print(recommendation_data.shape)
    # vs.visualize_sparse_matrix()
    '''


if __name__ == '__main__':
    test_on_feature_data()